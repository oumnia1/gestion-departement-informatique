package com.m2i.dto;

import lombok.*;
/**
 *Cette class contient les informations relatives à une demande.
 * Elle utilise des annotations de Lombok pour générer automatiquement les accesseurs, les constructeurs...
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class DemandeDto {
    private Long idDemande;
    private UtilisateurDto demandeur;
    private UtilisateurDto approbateur;
    private Long idDemandeur;
    private Long idApprobateur;
    private Long idObjet;
    private String dateDemande;
    private String dateTraitement;
    private String type;
    private String statut;
    private String statutCible;
    private String description;
    private String nomDemandeur;
    private String nomApprobateur;
    private FournitureDto fourniture;
    private MaterielDto materiel;
    private Integer quantite;
}
