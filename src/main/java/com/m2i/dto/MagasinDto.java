package com.m2i.dto;

import lombok.*;

import java.util.List;
/**
 *Cette class contient les informations relatives à un magasin.
 * Elle utilise des annotations de Lombok pour générer automatiquement les accesseurs, les constructeurs...
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class MagasinDto {
    private Long idMagasin;
    private DepartementDto departement;
    private String nom;
}
