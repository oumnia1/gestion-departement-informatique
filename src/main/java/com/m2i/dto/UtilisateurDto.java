package com.m2i.dto;

import com.m2i.enums.Fonction;
import com.m2i.enums.Grade;
import lombok.*;

import java.util.List;

/**
 * Classe représentant les données d'un utilisateur pour l'application.
 * Utilise des annotations de Lombok pour générer automatiquement les getters, setters, constructeurs...
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class UtilisateurDto {
    private Long idUtilisateur;
    private String nom;
    private String prenom;
    private String cin;
    private String tel;
    private Integer numBureau;
    private List<RoleDto> roles;
    private String email;
    private String password;
    private Grade grade;
    private Fonction fonction;
    private boolean isAdmin;
}
