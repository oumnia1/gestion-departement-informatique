package com.m2i.dao;

import com.m2i.entities.Materiel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MaterielRepository extends JpaRepository<Materiel, Long> {
    /**
     * Chercher tout le matériels qui appartient à un utilisateur
     * @param idUtilisateur
     * @return
     */
    @Query("select m from Materiel m where m.utilisateur.idUtilisateur = :idUtilisateur")
    List<Materiel> findByUtilisateurId(Long idUtilisateur);

    /**
     * Chercher tout le matériel qui n'est pas affecter à un utilisateur
     * @return
     */

    @Query("select m from Materiel m where m.utilisateur is null")
    List<Materiel> findMaterielDisponible();

    /**
     * Chercher tout le matériel qui appartient à un magasin spécifique
     * @param id
     * @return
     */
    @Query("select m from Materiel m where m.magasin.idMagasin = :id")
    List<Materiel> findByMagasinId(Long id);

    /**
     * Chercher un materiel ou plusieurs par type (complet ou partiel)
     * @param type
     * @return List<Materiel>
     */
    @Query("select m from Materiel m where m.type like CONCAT('%', :type, '%')")
    List<Materiel> findByType(@Param("type") String type);

    /**
     * Supprimer un matériel
     * @param id
     */
    @Modifying
    @Query("delete from Demande d where d.materiel.idMateriel = :id ")
    void deleteDemandeByMateriel(@Param("id") Long id);

}
