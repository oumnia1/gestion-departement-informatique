package com.m2i.dao;

import com.m2i.entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
    /**
     * Chercher un utilisateur par nom (complet ou partiel)
     * @param nom
     * @return List<Utilisateur>
     */
    @Query("select e from Utilisateur e where e.nom like CONCAT('%', :nom, '%')")
    List<Utilisateur> findByName(@Param("nom") String nom);

    /**
     * Chercher un Utilisateur par son email (utiliser dans la partie sécurité)
     * @param email
     * @return Utilisateur
     */
    Utilisateur findUtilisateurByEmail(String email);

    /**
     * (Pour l'utilisation de spring security)
     * Chercher un Utilisateur par son email (utiliser dans la partie sécurité)
     * @param username
     * @return Utilisateur
     */
    @Query("select e from Utilisateur e where e.email like :username")
    Utilisateur loadUserByUsername(String username);

    /**
     * Supprimer un utilisateur
     * @param id
     */
    @Modifying
    @Query("delete from Utilisateur u where u.idUtilisateur=:id")
    void delteUserCustom(Long id);

    /**
     * Supprimer une demande
     * @param id
     */
    @Modifying
    @Query("delete from Demande d where d.demandeur.idUtilisateur = :id " +
            "OR d.approbateur.idUtilisateur = :id")
    void deleteDemandeByUtilisateur(@Param("id") Long id);

    @Modifying
    @Query("update Utilisateur u set u.password = :password where u.idUtilisateur = :idUtilisateur")
    void updatePassword(@Param("idUtilisateur") Long idUtilisateur, @Param("password") String password);
}
