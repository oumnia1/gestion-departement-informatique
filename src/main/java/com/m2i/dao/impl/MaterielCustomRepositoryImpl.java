package com.m2i.dao.impl;

import com.m2i.dao.MaterielCustomRepository;
import com.m2i.dao.MaterielRepository;
import com.m2i.dao.UtilisateurRepository;
import com.m2i.entities.Materiel;
import com.m2i.entities.Utilisateur;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public class MaterielCustomRepositoryImpl implements MaterielCustomRepository {

    private final MaterielRepository materielRepository;
    private final UtilisateurRepository utilisateurRepository;

    public MaterielCustomRepositoryImpl(MaterielRepository materielRepository,
                                        UtilisateurRepository utilisateurRepository) {
        this.materielRepository = materielRepository;
        this.utilisateurRepository = utilisateurRepository;
    }


    @Override
    public Materiel affecterMateriel(Long idMateriel, Long idUtilisateur) {
        Utilisateur utilisateur = utilisateurRepository.findById(idUtilisateur).orElse(null);
        Materiel materiel = materielRepository.findById(idMateriel).orElse(null);
        materiel.setDateAffectation(LocalDateTime.now());
        materiel.setUtilisateur(utilisateur);
        return materielRepository.save(materiel);
    }
}
