package com.m2i.dao;

import com.m2i.entities.Departement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DepartementRepository extends JpaRepository<Departement, Long> {
    /**
     *  Chercher un département dont l'utilisateur avec l'ID fourni est cheou adjoint
     *  ou adjoint
     *  ou technicien
     * @param idUtilisateur
     * @return
     */
    @Query("select d from Departement d where d.chef.idUtilisateur = :idUtilisateur " +
            "or d.adjoint.idUtilisateur = :idUtilisateur " +
            "or d.technicien.idUtilisateur = :idUtilisateur")
    Departement findByUtilisateurId(Long idUtilisateur);
}
