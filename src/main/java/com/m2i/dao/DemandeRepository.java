package com.m2i.dao;

import com.m2i.entities.Demande;
import com.m2i.enums.StatutDemande;
import com.m2i.enums.TypeDemande;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DemandeRepository extends JpaRepository<Demande,Long> {
    /**
     * Chercher une liste de demandes réalisées par un utilisateur
     * @param idUser
     * @param type
     * @param objectId
     * @return
     */
    @Query("select d from Demande d where d.demandeur.idUtilisateur = :idUser and d.type = :type " +
            "and (d.fourniture.idFourniture = :objectId or d.materiel.idMateriel = :objectId)")
    List<Demande> findDemandeByUserAndTypeAndObject(@Param("idUser") Long idUser,
                                                        @Param("type") TypeDemande type,
                                                        @Param("objectId") Long objectId);

    /**
     * sélectionner les Demandes dont le statut correspond à celui fourni dans le paramètre
     * @param statut
     * @return
     */
    @Query("select d from Demande d where d.statut = :statut")
    List<Demande> findByStatut(@Param("statut") StatutDemande statut);

    /**
     * Chercher une liste de demandes réalisées par un utilisateur donné
     * @param id
     * @return
     */
    @Query("select d from Demande d where d.demandeur.idUtilisateur = :id")
    List<Demande> findByDemandeur(@Param("id") Long id);

}
