package com.m2i.controller;

import com.m2i.dto.DemandeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("Demande endpoint")
@RequestMapping("/demande")
public interface DemandeController {
    /**
     * Chercher un matériel par son ID
     * @param id
     * @return Fourniture DTO
     */
    @ApiOperation(value = "Chercher une demande par ID")
    @GetMapping(value = "/{id}}", produces = "application/json")
    ResponseEntity<DemandeDto> findById(@PathVariable("id") Long id);

    /**
     * Récuperer tout le matériel du département
     * @return ResponseEntity<List<FournitureDto>>
     */
    @ApiOperation(value = "Chercher la liste de toutes les demandes")
    @GetMapping(value = "/all", produces = "application/json")
    ResponseEntity<List<DemandeDto>> findAll();

    /**
     * Chercher une liste de demandes par statut
     * @param statut
     * @return
     */
    @ApiOperation(value = "Chercher une liste de demandes par statut")
    @GetMapping(value = "/statut/{statut}", produces = "application/json")
    ResponseEntity<List<DemandeDto>> findByStatut(@PathVariable("statut") String statut);

    /**
     * Chercher la liste des demandes d'un utilisateur
     * @param id
     * @return
     */
    @ApiOperation(value = "Chercher la liste des demandes d'un utilisateur")
    @GetMapping(value = "/demandeur/{id}", produces = "application/json")
    ResponseEntity<List<DemandeDto>> findByDemandeur(@PathVariable("id") Long id);

    /**
     * Ajouter une demande
     * @param dto
     * @return
     */
    @ApiOperation(value = "Ajouter une demande")
    @PostMapping(value = "/add", produces = "application/json")
    ResponseEntity<Object> save(@RequestBody DemandeDto dto);

    /**
     * Modifier une demande
     * @param dto
     * @return
     */
    @ApiOperation(value = "Modifier une demande")
    @PutMapping(value = "/update", produces = "application/json")
    ResponseEntity<DemandeDto> update(@RequestBody DemandeDto dto);

    /**
     * Supprimer une demande
     * @param id
     * @return
     */
    @ApiOperation(value = "Supprimer une demande")
    @DeleteMapping(value = "/delete/{id}", produces = "application/json")
    ResponseEntity<DemandeDto> delete(@PathVariable("id") Long id);

    /**
     * Traitement d'une demande
     * @param dto
     * @return
     */
    @ApiOperation(value = "Traiter une demande (Approuver, rejter, ...)")
    @PutMapping(value = "/traiter", produces = "application/json")
    ResponseEntity<Object> traiter(@RequestBody DemandeDto dto);
}
