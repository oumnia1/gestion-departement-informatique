package com.m2i.controller;

import com.m2i.dto.FournitureDto;
import com.m2i.dto.MaterielDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("Fourniture endpoint")
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/fourniture")
public interface FournitureController {
    /**
     * Chercher une fourniture par son ID
     * @param id
     * @return Fourniture DTO
     */
    @ApiOperation(value = "Chercher une fourniture par son ID")
    @GetMapping(value = "/{id}", produces = "application/json")
    ResponseEntity<FournitureDto> findById(@PathVariable("id") Long id);

    /**
     * Ajouter une fourniture
     * @param dto
     * @return
     */
    @ApiOperation(value = "Ajouter une fourniture")
    @PostMapping(value = "/add", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<FournitureDto> save(@RequestBody FournitureDto dto);

    /**
     * Supprimer une fourniture
     * @param id
     * @return
     */
    @ApiOperation(value = "Ajouter une fourniture")
    @DeleteMapping(value = "/delete/{id}", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<FournitureDto> delete(@PathVariable("id") Long id);

    /**
     * Récuperer toutes les foutrnitures du département
     * @return ResponseEntity<List<FournitureDto>>
     */
    @ApiOperation(value = "Récuperer toutes les foutrnitures du département")
    @GetMapping(value = "/all", produces = "application/json")
    ResponseEntity<List<FournitureDto>> findAll();

    /**
     * Récuperer un ou plusieurs fourniture par type
     * @return ResponseEntity<List<FournitureDto>>
     */
    @ApiOperation(value = "Récuperer un ou plusieurs fourniture par type")
    @GetMapping(value = "/type/{type}", produces = "application/json")
    ResponseEntity<List<FournitureDto>> findByType(@PathVariable("type") String type);

    /**
     * Modifier une fourniture
     * @return ResponseEntity<FournitureDto>
     */
    @ApiOperation(value = "Modifier une fourniture")
    @PutMapping(value = "/update", produces = "application/json")
    ResponseEntity<FournitureDto> update(@RequestBody FournitureDto dto);
}
