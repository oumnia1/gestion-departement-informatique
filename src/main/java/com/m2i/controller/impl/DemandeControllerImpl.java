package com.m2i.controller.impl;

import com.m2i.controller.DemandeController;
import com.m2i.dto.DemandeDto;
import com.m2i.dto.UtilisateurDto;
import com.m2i.service.DemandeService;
import com.m2i.utils.ApiError;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class DemandeControllerImpl implements DemandeController {

    private final DemandeService demandeService;

    @Override
    public ResponseEntity<DemandeDto> findById(Long id) {
        DemandeDto resultat = demandeService.findById(id);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<List<DemandeDto>> findAll() {
        List<DemandeDto> resultat = demandeService.findAll();
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<List<DemandeDto>> findByStatut(String statut) {
        List<DemandeDto> resultat = demandeService.findByStatut(statut);
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<List<DemandeDto>> findByDemandeur(Long id) {
        List<DemandeDto> resultat = demandeService.findByDemandeur(id);
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<Object> save(DemandeDto dto) {
        try{
            DemandeDto result = demandeService.save(dto);
            return ResponseEntity.ok(result);
        } catch (RuntimeException e) {
            ApiError apiError =
                    new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), e.getMessage());
            return new ResponseEntity<Object>(
                    apiError, new HttpHeaders(), apiError.getStatus());
        }
    }

    @Override
    public ResponseEntity<DemandeDto> update(DemandeDto dto) {
        return null;
    }

    @Override
    public ResponseEntity<DemandeDto> delete(Long id) {
        DemandeDto demande = demandeService.findById(id);
        demandeService.delete(id);
        return ResponseEntity.ok(demande);
    }

    @Override
    public ResponseEntity<Object> traiter(DemandeDto dto) {
        try{
            DemandeDto demande = demandeService.findById(dto.getIdDemande());
            demandeService.traiterDemande(dto);
            return ResponseEntity.ok(demande);
        } catch (RuntimeException e) {
            ApiError apiError =
                    new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), e.getMessage());
            return new ResponseEntity<Object>(
                    apiError, new HttpHeaders(), apiError.getStatus());
        }
    }
}
