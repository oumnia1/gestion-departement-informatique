package com.m2i.controller.impl;

import com.m2i.controller.HomeController;
import org.springframework.stereotype.Controller;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Default implementation of the {@link HomeController}
 */
@ApiIgnore
@Controller
class HomeControllerImpl implements HomeController {

    @Override
    public String redirectToSwaggerUi() {
        return "redirect:/swagger-ui/";
    }

}
