package com.m2i.controller.impl;

import com.m2i.controller.UtilisateurController;
import com.m2i.dto.UtilisateurDto;
import com.m2i.dto.DepartementDto;
import com.m2i.service.UtilisateurService;
import com.m2i.utils.ApiError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UtilisateurControllerImpl implements UtilisateurController {

    private final UtilisateurService utilisateurService;

    public UtilisateurControllerImpl(UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    @Override
    public ResponseEntity<UtilisateurDto> findById(Long id) {
        UtilisateurDto resultat = utilisateurService.findById(id);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<List<UtilisateurDto>> findAll() {
        List<UtilisateurDto> resultat = utilisateurService.findByAll();
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<Object> save(UtilisateurDto dto) {
        try{
            UtilisateurDto resultat = utilisateurService.save(dto);
            return ResponseEntity.ok(resultat);
        } catch (RuntimeException e) {
            ApiError apiError =
                    new ApiError(HttpStatus.BAD_REQUEST,
                            e.getLocalizedMessage(), "Un utilisateur existe déjà avec cet email");
            return new ResponseEntity<Object>(
                    apiError, new HttpHeaders(), apiError.getStatus());
        }
    }

    @Override
    public ResponseEntity<UtilisateurDto> update(UtilisateurDto dto) {
        UtilisateurDto resultat = utilisateurService.update(dto);
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<UtilisateurDto> delete(Long id) {
        UtilisateurDto user = utilisateurService.findById(id);
        utilisateurService.delete(id);
        return ResponseEntity.ok(user);
    }

    @Override
    public ResponseEntity<List<UtilisateurDto>> findByName(String nom) {
        List<UtilisateurDto> resultat = utilisateurService.findByName(nom);
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<UtilisateurDto> findUtilisateurByEmail(String email) {
        UtilisateurDto resultat = utilisateurService.findUtilisateurByEmail(email);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<DepartementDto> changerChef(Long idDepartement, Long idChef) {
        DepartementDto resultat = utilisateurService.updateChefDepartement(idDepartement,idChef);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<DepartementDto> changerAdjoint(Long idDepartement, Long idAdjoint) {
        DepartementDto resultat = utilisateurService.updateChefAdjointDepartement(idDepartement,idAdjoint);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<UtilisateurDto> updatePassword(UtilisateurDto dto) {
        UtilisateurDto resultat = utilisateurService.updatePassword(dto);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }
}
