package com.m2i.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.m2i.dao.UtilisateurRepository;
import com.m2i.entities.Utilisateur;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    /**
     *(AuthenticationManager)cette interface est utilisée pour authentifier les utilisateurs en vérifiant leurs informations d'identification.
     */
    private final AuthenticationManager authenticationManager;

    private final UtilisateurRepository utilisateurRepository;

    /**
     * Constructeur de la classe
     * @param authenticationManager
     * @param utilisateurRepository
     */

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, UtilisateurRepository utilisateurRepository) {
        this.authenticationManager = authenticationManager;
        this.utilisateurRepository = utilisateurRepository;
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        //Stocker les informations d'identification de l'utilisateur
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(email,password);
        return authenticationManager.authenticate(authenticationToken);
    }

    /**
     * Cette méthode est appelée lorsque l'authentification a réussi,pour gérer les Token
     * @param request
     * @param response
     * @param chain
     * @param authResult
     * @throws IOException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult)
            throws IOException {

        User user = (User) authResult.getPrincipal();
        Algorithm algorithm = Algorithm.HMAC256("SECRET");

        String accessToken = JWT.create().withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+(24*60*60*1000))) // 24 heures
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles",user.getAuthorities().stream()
                        .map(element -> element.getAuthority()).collect(Collectors.toList()))
                .sign(algorithm);

        String refreshToken = JWT.create().withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+(30*24*60*60*1000))) // 30 jours
                .withIssuer(request.getRequestURL().toString())
                .sign(algorithm);

        Map<String,Object> userInfo = new HashMap<>();
        userInfo.put("access_token",accessToken);
        userInfo.put("refresh_token",refreshToken);
        userInfo.put("email",user.getUsername());

        // Add roles
        List<String> roles = new ArrayList<>();
        user.getAuthorities().forEach(grantedAuthority -> roles.add(grantedAuthority.getAuthority()));
        userInfo.put("roles",roles);


        response.setContentType("application/json");

        Utilisateur utilisateur = utilisateurRepository.findUtilisateurByEmail(user.getUsername());

        userInfo.put("id",utilisateur.getIdUtilisateur());
        userInfo.put("nom",utilisateur.getNom());
        userInfo.put("prenom",utilisateur.getPrenom());
        new ObjectMapper().writeValue(response.getOutputStream(),userInfo);
    }

}
