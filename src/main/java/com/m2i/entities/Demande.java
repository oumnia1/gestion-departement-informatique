package com.m2i.entities;

import com.m2i.enums.StatutDemande;
import com.m2i.enums.TypeDemande;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "DEMANDE")
/**
 * Représentation d'un objet demande
 */
public class Demande {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idDemande;

    @Column(name = "DATE_DEMANDE")
    private LocalDateTime dateDemande;

    /**
     * Encours
     * Approuvee
     * Rejetee
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUT")
    private StatutDemande statut;

    /**
     * le type du demande soit:
     * Fourniture
     * Matériel
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE")
    private TypeDemande type;

    @Column(name = "DESCRIPTION")
    private String description;

    /**
     * un seul utilisateur peut faire plusieurs demandes
     */
    @ManyToOne
    @JoinColumn(name = "DEMANDEUR")
    private Utilisateur demandeur;

    /**
     * ce qui a approuver la demande(Chef ou Adjoint)
     */
   @ManyToOne
    @JoinColumn(name = "APPROBATEUR")
    private Utilisateur approbateur;

    @Column(name = "DATE_TRAITEMENT")
    private LocalDateTime dateTraitement;


    @OneToOne
    @JoinColumn(name = "FOURNITURE")
    private Fourniture fourniture;

    @OneToOne
    @JoinColumn(name = "MATERIEL")
    private Materiel materiel;

    @Column(name = "QUANTITE")
    private Integer quantite;
}
