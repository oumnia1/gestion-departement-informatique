package com.m2i.entities;

import javax.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "DEPARTEMENT")
/**
 * Représentation d'un objet departement
 */
public class Departement {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long idDepartement;

    /**
     * un seul chef peut etre affecter à un seul  departement
     */
    @OneToOne
    @JoinColumn( name="CHEF" )
    private Utilisateur chef;

    /**
     * un seul Adjoint peut etre affecter à un seul  departement
     */
    @OneToOne
    @JoinColumn( name="ADJOINT" )
    private Utilisateur adjoint;

    /**
     * un seul technicien peut etre affecter à un seul  departement
     */
    @OneToOne
    @JoinColumn( name="TECHNICIEN" )
    private Utilisateur technicien;

    /**
     * Nom departement(dans notre cas:informatique)
     */
    @Column(name = "NOM")
    private String nom;


   /** @OneToMany
    @JoinTable( name = "T_DEP_ENS",
            joinColumns = @JoinColumn( name = "idDepartement" ),
            inverseJoinColumns = @JoinColumn( name = "idUtilisateur" ) )
    private List<Utilisateur> enseignants; */

}
