package com.m2i.entities;

import com.m2i.enums.Fonction;
import com.m2i.enums.Grade;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "UTILISATEUR")
/**
 * Représentation d'un objet utilisateur
 */
public class Utilisateur {
    /**
     * la clé primaire est id
     * il sera générer authomatiquement
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idUtilisateur;

    @Column(name = "NOM")
    private String nom;

    @Column(name = "PRENOM")
    private String prenom;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "CIN")
    private String cin;

    @Column(name = "TEL")
    private String tel;

    @Column(name = "NUM_BUREAU")
    private Integer numBureau;

    /**
     * Champ de type enumeration
     */
    @Column(name = "FONCTION")
    @Enumerated(EnumType.STRING)
    private Fonction fonction;

    @Column(name = "GRADE")
    @Enumerated(EnumType.STRING)
    private Grade grade;

    /**
     *les rôles associés à un utilisateur doivent être chargés immédiatement lorsque l'entité
     utilisateur est récupérée à partir de la base de données.
     */
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name = "T_UTI_ROLE",
            joinColumns = @JoinColumn( name = "idUtilisateur" ),
            inverseJoinColumns = @JoinColumn( name = "idRole" ) )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Role> roles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinTable( name = "T_UTI_DEM",
            joinColumns = @JoinColumn( name = "idUtilisateur" ),
            inverseJoinColumns = @JoinColumn( name = "idDemande" ) )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Demande> demandes;
}
