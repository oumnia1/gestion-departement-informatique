package com.m2i.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "MAGASIN")
/**
 * Représentation d'un objet magasin
 */
public class Magasin {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idMagasin;

    /**
     * une seule instance de la classe magasin peut être liée à un seul objet de type Département
     */
    @OneToOne
    @JoinColumn( name="DEPARTEMENT")
    private Departement departement;

    @Column( name="NOM" )
    private String nom;
}
