package com.m2i.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "MATERIEL")
/**
 * Représentation d'un objet matériel
 */
public class Materiel {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idMateriel;

    @Column(name = "NUM_INV")
    private Integer numeroInventaire;

    /**
     * le nom du matériel
     */
    @Column(name = "TYPE")
    private String type;

    @Column(name = "DATE_ACQ")
    private LocalDateTime dateAcquisition;

    /**
     * Plusieurs matériel peut etre affecter à un seul utilisateur
     */
    @ManyToOne
    @JoinColumn( name="AFFECTATION")
    private Utilisateur utilisateur;

    @Column(name = "DATE_AFFECTATION")
    private LocalDateTime dateAffectation;

    /**
     * Plusieurs matériel peut etre affecter à un seul magasin
     */
    @ManyToOne
    @JoinColumn( name="idMagasin")
    /**
     *  ce champ ne devrait pas être inclus lors de la conversion d'un objet en un format JSON
     */
    @JsonIgnore
    private Magasin magasin;
}
