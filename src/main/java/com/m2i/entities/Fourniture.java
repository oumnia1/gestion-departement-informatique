package com.m2i.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "FOURNITURE")
/**
 * Représentation d'un objet fourniture
 */
public class Fourniture {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idFourniture;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "QUANTITE")
    private Integer quantite;

    /**
     * plusieurs fourniture peut etre affecter à un seul magasin
     */
    @ManyToOne
    @JoinColumn( name="idMagasin")
    @JsonIgnore
    private Magasin magasin;
}
