package com.m2i.entities;

import com.m2i.enums.RoleName;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "ROLE")
/**
 * Representation d'un objet Role
 */
public class Role {
    /**
     * la clé primaire est id
     * il va être générer authomatiquement
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idRole;

    /**
     * Définit le nom du role soit ADMINISTRATEUR soit UTILISATEUR
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "NOM")
    private RoleName nom;
}
