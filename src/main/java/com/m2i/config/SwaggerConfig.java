package com.m2i.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 * Configuration de l'utilisation de Swagger pour la documentation de l'API.
 * La classe Docket est utilisée pour créer une instance de Swagger qui permet
 * de décrire et documenter les API de l'application. La méthode api() retourne
 * cette instance configurée avec la version 2 de Swagger (DocumentationType.SWAGGER_2).
 * L'annotation @EnableSwagger2 permet d'activer Swagger dans l'application, ce qui
 * permet de générer automatiquement la documentation de toutes les API.
 */


@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2);
    }
}
