package com.m2i.service;

import com.m2i.dto.MaterielDto;

import java.util.List;

public interface MaterielService {
    /**
     * Chercher le matériel par son id
     * @param id
     * @return
     */
    MaterielDto findById(Long id);

    /**
     * Ajouter un nouveau matériel
     * @param dto
     * @return
     */

    MaterielDto save(MaterielDto dto);

    /**
     * Supprimer un matériel de la base de donnée
     * @param id
     */

    void delete(Long id);

    /**
     * Affecter un matériel par un administrateur
     * @param idMateriel
     * @param idUtilisateur
     * @return
     */
    MaterielDto affecterMateriel(Long idMateriel, Long idUtilisateur);

    /**
     * Supprimer l'affectation d'un matériel
     * @param idUtilisateur
     */

    void deleteAffectationMateriel(Long idUtilisateur);

    /**
     * Chercher tout le matériel
     * @return
     */

    List<MaterielDto> findAll();

    /**
     * Chercher le matériel par son id Magasin
     * @param id
     * @return
     */

    List<MaterielDto> findByMagasinId(Long id);

    /**
     * Mise à jour d'un matériel
     * @param dto
     * @return
     */

    MaterielDto update(MaterielDto dto);

    /**
     * Chercher les matériel qui ne sont pas affecter aux autres utilisateurs
     * @return
     */
    List<MaterielDto> findMaterielDisponible();

    /**
     * Chercher un matériel par son type
     * @param type
     * @return
     */

    List<MaterielDto> findByType(String type);
}
