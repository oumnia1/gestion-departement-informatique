package com.m2i.service;

import com.m2i.dto.DepartementDto;
import com.m2i.dto.RoleDto;
import com.m2i.dto.UtilisateurDto;

import java.util.List;

public interface UtilisateurService {
    /**
     * Chercher un utilisateur par son ID
     * @param id
     * @return
     */
    UtilisateurDto findById(Long id);

    /**
     * Chercher tous les utilisateurs
     */
    List<UtilisateurDto> findByAll();

    /**
     * Ajouter un utilisateur
     * @param dto
     * @return
     */

    UtilisateurDto save(UtilisateurDto dto);

    /**
     * Modifier les informations d'un utilisateur
     * @param dto
     * @return
     */

    UtilisateurDto update(UtilisateurDto dto);

    /**
     * Supprimer un utilisateur
     * @param id
     */
    void delete(Long id);

    /**
     * chercher un utilisateur par son nom
     * @param nom
     * @return
     */
    List<UtilisateurDto> findByName(String nom);

    UtilisateurDto addRoles(Long idUtilisateur, List<RoleDto> roles);

    /**
     * chercher un utilisateur par son email
     * @param email
     * @return
     */
    UtilisateurDto findUtilisateurByEmail(String email);

    UtilisateurDto loadUserByUsername(String username);
    DepartementDto updateChefDepartement(Long idDepartement, Long idChef);
    DepartementDto updateChefAdjointDepartement(Long idDepartement, Long idAdjoint);

    /**
     * Changer le mot de passe
     * @param dto
     * @return UtilisateurDto
     */
    UtilisateurDto updatePassword(UtilisateurDto dto);
}
