package com.m2i.service;

import com.m2i.dto.FournitureDto;

import java.util.List;

public interface FournitureService {
    FournitureDto findById(Long id);

    /**
     * Ajouter une nouvelle fourniture
     * @param dto
     * @return
     */
    FournitureDto save(FournitureDto dto);

    /**
     * Supprimer une fourniture
     * @param id
     */
    void delete(Long id);

    /**
     * Chercher toutes les fournitures
     * @return
     */
    List<FournitureDto> findAll();

    /**
     * Chercher une fourniture par son type
     * @param type
     * @return
     */

    List<FournitureDto> findByType(String type);

    /**
     * Modifier les informations d'une fourniture
     * @param dto
     * @return
     */

    FournitureDto update(FournitureDto dto);
    void updateQuantite(Long id, Integer quantite);


}
