package com.m2i.service;

import com.m2i.dto.MagasinDto;

public interface MagasinService {
    MagasinDto findById(Long id);
    MagasinDto save(MagasinDto dto);
}
