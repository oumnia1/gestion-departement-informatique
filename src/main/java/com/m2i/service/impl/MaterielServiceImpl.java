package com.m2i.service.impl;

import com.m2i.dao.MagasinRepository;
import com.m2i.dao.MaterielCustomRepository;
import com.m2i.dao.MaterielRepository;
import com.m2i.dao.UtilisateurRepository;
import com.m2i.dto.MaterielDto;
import com.m2i.entities.Materiel;
import com.m2i.entities.Utilisateur;
import com.m2i.service.DemandeService;
import com.m2i.service.MaterielService;
import com.m2i.utils.MagasinUtils;
import com.m2i.utils.MaterielUtils;
import com.m2i.utils.UtilisateurUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MaterielServiceImpl implements MaterielService {

    private final MaterielRepository materielRepository;

    private final MaterielCustomRepository materielCustomRepository;

    private final UtilisateurRepository utilisateurRepository;

    private final MagasinRepository magasinRepository;

    public MaterielServiceImpl(MaterielRepository materielRepository, MaterielCustomRepository materielCustomRepository, UtilisateurRepository utilisateurRepository, MagasinRepository magasinRepository) {
        this.materielRepository = materielRepository;
        this.materielCustomRepository = materielCustomRepository;
        this.utilisateurRepository = utilisateurRepository;
        this.magasinRepository = magasinRepository;
    }

    @Override
    public MaterielDto findById(Long id) {
        return MaterielUtils.getInstance().mapEntityToDto(materielRepository.findById(id).orElse(null));
    }

    @Override
    /**
     * si l'ID de l'affectation de l'objet n'est pas vide, elle trouve l'utilisateur correspondant à cet ID dans la base de données et l'ajoute à l'objet "MaterielDto"
     * Sinon, elle met la date d'affectation à null
     */
    public MaterielDto save(MaterielDto dto) {
        if(dto.getIdAffectation() != null && !dto.getIdAffectation().isBlank()){
            Utilisateur user = utilisateurRepository.findById(Long.valueOf(dto.getIdAffectation())).orElse(null);
            dto.setUtilisateur(UtilisateurUtils.getInstance().mapEntityToDto(user));
        } else {
            dto.setDateAffectation(null);
        }
        if(dto.getMagasin() == null){
            // Magasin departement informatique par defaut
            dto.setMagasin(MagasinUtils.getInstance().mapEntityToDto(
                    magasinRepository.findById(1L).orElse(null)
            ));
        }
        return MaterielUtils.getInstance().mapEntityToDto(
                materielRepository.save(MaterielUtils.getInstance().mapDtoToEntity(dto)));
    }

    @Override
    public void delete(Long id) {
        materielRepository.deleteDemandeByMateriel(id);
        materielRepository.deleteById(id);
    }

    @Override
    public MaterielDto affecterMateriel(Long idMateriel, Long idUtilisateur) {
        return MaterielUtils.getInstance()
                .mapEntityToDto(materielCustomRepository.affecterMateriel(idMateriel,idUtilisateur));
    }

    @Override
    public void deleteAffectationMateriel(Long idUtilisateur) {
        List<Materiel> materiels = materielRepository.findByUtilisateurId(idUtilisateur);
        if(materiels != null){
            materiels.forEach(materiel -> materiel.setUtilisateur(null));
            materielRepository.saveAllAndFlush(materiels);
        }
    }

    @Override
    public List<MaterielDto> findAll() {
        return MaterielUtils.getInstance()
                .mapEntityToDtoList(materielRepository.findAll());
    }

    @Override
    public List<MaterielDto> findByMagasinId(Long id) {
        return MaterielUtils.getInstance()
                .mapEntityToDtoList(materielRepository.findByMagasinId(id));
    }

    @Override
    public MaterielDto update(MaterielDto dto) {
        Utilisateur user = null;
        Materiel entity = materielRepository.findById(dto.getIdMateriel()).orElse(null);
        if(dto.getIdAffectation() != null && !dto.getIdAffectation().isBlank()){
            user = utilisateurRepository.findById(Long.valueOf(dto.getIdAffectation())).orElse(null);
        }
        MaterielDto materiel = MaterielUtils.getInstance().populateDto(dto,entity);
        materiel.setUtilisateur(UtilisateurUtils.getInstance().mapEntityToDto(user));
        return save(materiel);
    }

    @Override
    public List<MaterielDto> findMaterielDisponible() {
        return MaterielUtils.getInstance().mapEntityToDtoList(materielRepository.findMaterielDisponible());
    }

    @Override
    public List<MaterielDto> findByType(String type) {
        return MaterielUtils.getInstance().mapEntityToDtoList(materielRepository.findByType(type));
    }
}
