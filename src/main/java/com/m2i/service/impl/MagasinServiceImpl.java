package com.m2i.service.impl;

import com.m2i.dao.MagasinRepository;
import com.m2i.dto.MagasinDto;
import com.m2i.service.MagasinService;
import com.m2i.utils.MagasinUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MagasinServiceImpl implements MagasinService {
    private final MagasinRepository magasinRepository;

    public MagasinServiceImpl(MagasinRepository magasinRepository) {
        this.magasinRepository = magasinRepository;
    }

    @Override
    public MagasinDto findById(Long id) {
        return MagasinUtils.getInstance().mapEntityToDto(magasinRepository.findById(id).orElse(null));
    }

    @Override
    public MagasinDto save(MagasinDto dto) {
        return MagasinUtils.getInstance().mapEntityToDto(
                magasinRepository.save(MagasinUtils.getInstance().mapDtoToEntity(dto)));
    }
}
