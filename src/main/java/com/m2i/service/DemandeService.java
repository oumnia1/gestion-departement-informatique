package com.m2i.service;

import com.m2i.dto.DemandeDto;

import java.util.List;

public interface DemandeService {
    DemandeDto findById(Long id);

    /**
     * Chercher toutes les demandes
     * @return
     */
    List<DemandeDto> findAll();

    /**
     * Chercher la demande par son statut
     * @param statut
     * @return
     */
    List<DemandeDto> findByStatut(String statut);

    /**
     * Chercher la demande par son demandeur
     * @param id
     * @return
     */
    List<DemandeDto> findByDemandeur(Long id);

    /**
     * Faire une demande
     * @param dto
     * @return
     */
    DemandeDto save(DemandeDto dto);
    DemandeDto update(DemandeDto dto);

    /**
     * Traitement de la demande par l'administrateur
     * @param dto
     */
    void traiterDemande(DemandeDto dto);

    /**
     * Annuler la demande
     * @param id
     */
    void delete(Long id);
}
