package com.m2i.utils;

import com.m2i.dto.DepartementDto;
import com.m2i.entities.Departement;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton en charge des traitements techniques de l'entité et DTO Departement
 */
public class DepartementUtils {
    private static DepartementUtils instance = null;

    private DepartementUtils() {
    }

    public static DepartementUtils getInstance() {
        if (instance == null)
            instance = new DepartementUtils();

        return instance;
    }

    /**
     * Mapper (convertit) un objet Departement en DepartementDto
     * @param entity
     * @return
     */

    public DepartementDto mapEntityToDto(Departement entity){
        if(entity == null){
            return null;
        }

        return DepartementDto.builder()
                .idDepartement(entity.getIdDepartement())
                .nom(entity.getNom())
                .chef(UtilisateurUtils.getInstance().mapEntityToDto(entity.getChef()))
                .adjoint(UtilisateurUtils.getInstance().mapEntityToDto(entity.getAdjoint()))
                .technicien(UtilisateurUtils.getInstance().mapEntityToDto(entity.getTechnicien()))
               // .enseignants(UtilisateurUtils.getInstance().mapEntityLToDtoList(entity.getEnseignants()))
                //.magasin(MagasinUtils.getInstance().mapEntityToDto(entity.getMagasin()))
                .build();
    }

    /**
     * Mapper (convertit) un objet DepartementDto en Departement
     * @param dto
     * @return
     */

    public Departement mapDtoToEntity(DepartementDto dto){
        if(dto == null){
            return null;
        }
        return Departement.builder()
                .idDepartement(dto.getIdDepartement())
                .nom(dto.getNom())
                .chef(UtilisateurUtils.getInstance().mapDtoToEntity(dto.getChef()))
                .adjoint(UtilisateurUtils.getInstance().mapDtoToEntity(dto.getAdjoint()))
                .technicien(UtilisateurUtils.getInstance().mapDtoToEntity(dto.getTechnicien()))
                //.enseignants(UtilisateurUtils.getInstance().mapDtoToEntityList(dto.getEnseignants()))
                //.magasin(MagasinUtils.getInstance().mapDtoToEntity(dto.getMagasin()))
                .build();
    }

    /**
     * Mapper (convertit) une liste d'objets Departement en une liste d'objets DepartementDto
     * @param entities
     * @return
     */
    public List<DepartementDto> mapEntityLToDtoList(List<Departement> entities){
        if(entities == null){
            return new ArrayList<>();
        }
        List<DepartementDto> dtos = new ArrayList<>();
        entities.forEach(element -> dtos.add(mapEntityToDto(element)));
        return dtos;
    }

    /**
     * Mapper (convertit) une liste d'objets DepartementDto en une liste d'objets Departement
     * @param dtos
     * @return
     */

    public List<Departement> mapDtoToEntityList(List<DepartementDto> dtos){
        if(dtos == null){
            return new ArrayList<>();
        }
        List<Departement> entities = new ArrayList<>();
        dtos.forEach(element -> entities.add(mapDtoToEntity(element)));
        return entities;
    }
}
