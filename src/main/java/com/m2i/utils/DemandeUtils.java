package com.m2i.utils;

import com.m2i.dto.DemandeDto;
import com.m2i.entities.Demande;
import com.m2i.enums.StatutDemande;
import com.m2i.enums.TypeDemande;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Singleton en charge des traitements techniques de l'entité et DTO Demande
 */
public class    DemandeUtils {
    private static DemandeUtils instance = null;

    /**
     * Formater des dates pour l'affichage (Front-end)
     */
    private final DateTimeFormatter formattersFront = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    /**
     * Formater des dates pour l'enregistrement dans une base de données.
     */
    private final DateTimeFormatter formattersDB = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * Constructeur privé pour empêcher la création d'instances de la classe en dehors de la classe elle-même
     */
    private DemandeUtils() {
    }

    public static DemandeUtils getInstance() {
        if (instance == null)
            instance = new DemandeUtils();

        return instance;
    }

    /**
     * Mapper (convertit) un objet Demande en DemandeDto
     * @param entity
     * @return
     */
    public DemandeDto mapEntityToDto(Demande entity){
        if(entity == null){
            return null;
        }
        String dateDemande = entity.getDateDemande().format(formattersFront);
        String dateTraitement = entity.getDateTraitement() != null ?
                entity.getDateTraitement().format(formattersFront) : null;
        return DemandeDto.builder()
                .idDemande(entity.getIdDemande())
                .nomDemandeur(entity.getDemandeur().getNom() +" "+ entity.getDemandeur().getPrenom())
                .type(entity.getType().name())
                .dateDemande(dateDemande)
                .statut(entity.getStatut().name())
                .dateTraitement(dateTraitement)
                .nomApprobateur(entity.getApprobateur() != null ? entity.getApprobateur().getNom() +" "+ entity.getApprobateur().getPrenom() : null)
                .demandeur(UtilisateurUtils.getInstance().mapEntityToDto(entity.getDemandeur()))
                .approbateur(UtilisateurUtils.getInstance().mapEntityToDto(entity.getApprobateur()))
                .materiel(entity.getMateriel() != null ? MaterielUtils.getInstance().mapEntityToDto(entity.getMateriel()) : null)
                .fourniture(entity.getFourniture() != null ? FournitureUtils.getInstance().mapEntityToDto(entity.getFourniture()) : null)
                .description(entity.getDescription())
                .build();
    }

    /**
     * Mapper (convertit) un objet DemandeDto en Demande
     * @param dto
     * @return
     */

    public Demande mapDtoToEntity(DemandeDto dto){
        if(dto == null){
            return null;
        }
        LocalDateTime dateDemande = LocalDateTime.parse(dto.getDateDemande());
        LocalDateTime dateTraitement = dto.getDateTraitement() != null ?
                LocalDateTime.parse(dto.getDateTraitement(), formattersDB) : null;

        return Demande.builder()
                .idDemande(dto.getIdDemande())
                .type(TypeDemande.valueOf(dto.getType()))
                .statut(StatutDemande.valueOf(dto.getStatut()))
                .dateDemande(dateDemande)
                .dateTraitement(dateTraitement)
                .description(dto.getDescription())
                .demandeur(UtilisateurUtils.getInstance().mapDtoToEntity(dto.getDemandeur()))
                .approbateur(UtilisateurUtils.getInstance().mapDtoToEntity(dto.getApprobateur()))
                .materiel(dto.getMateriel() != null ? MaterielUtils.getInstance().mapDtoToEntity(dto.getMateriel()) : null)
                .fourniture(dto.getFourniture() != null ? FournitureUtils.getInstance().mapDtoToEntity(dto.getFourniture()) : null)
                .quantite(dto.getQuantite())
                .build();
    }

    /**
     * Mapper (convertit) une liste d'objets Demande en une liste d'objets DemandeDto
     * @param entities
     * @return
     */

    public List<DemandeDto> mapEntityToDtoList(List<Demande> entities){
        if(entities == null){
            return null;
        }
        List<DemandeDto> dtos = new ArrayList<>();
        entities.forEach(element -> dtos.add(mapEntityToDto(element)));
        return dtos;
    }

    /**
     * Mapper (convertit) une liste d'objets DemandeDto en une liste d'objets Demande
     * @param dtos
     * @return
     */

    public List<Demande> mapDtoToEntityList(List<DemandeDto> dtos){
        if(dtos == null){
            return null;
        }
        List<Demande> entities = new ArrayList<>();
        dtos.forEach(element -> entities.add(mapDtoToEntity(element)));
        return entities;
    }

    /**
     * Methode qui prend en entrée un objet DemandeDto et un objet Demande et remplit les champs manquants de l'objet DemandeDto avec les données de l'objet Demande
     * @param dto
     * @param entity
     * @return
     */
    public DemandeDto populateDto(DemandeDto dto, Demande entity){
        if(entity == null){
            return null;
        }

        return dto;
    }
}
