package com.m2i.utils;

import com.m2i.dto.FournitureDto;
import com.m2i.dto.MaterielDto;
import com.m2i.entities.Fourniture;
import com.m2i.entities.Materiel;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton en charge des traitements techniques de l'entité et DTO Fourniture
 */
public class FournitureUtils {
    private static FournitureUtils instance = null;

    /**
     * Constructeur privé pour empêcher la création d'instances de la classe en dehors de la classe elle-même
     */
    private FournitureUtils() {
    }

    public static FournitureUtils getInstance() {
        if (instance == null)
            instance = new FournitureUtils();

        return instance;
    }

    /**
     * Mapper (convertit) un objet Fourniture en FournitureDto
     * @param entity
     * @return
     */
    public FournitureDto mapEntityToDto(Fourniture entity){
        if(entity == null){
            return null;
        }
        return FournitureDto.builder()
                .idFourniture(entity.getIdFourniture())
                .type(entity.getType())
                .quantite(entity.getQuantite())
                .magasin(MagasinUtils.getInstance().mapEntityToDto(entity.getMagasin()))
                .build();
    }

    /**
     * Mapper (convertit) un objet FournitureDto en Fourniture
     * @param dto
     * @return
     */

    public Fourniture mapDtoToEntity(FournitureDto dto){
        if(dto == null){
            return null;
        }
        return Fourniture.builder()
                .idFourniture(dto.getIdFourniture())
                .type(dto.getType())
                .quantite(dto.getQuantite())
                .magasin(MagasinUtils.getInstance().mapDtoToEntity(dto.getMagasin()))
                .build();
    }

    /**
     * Mapper (convertit) une liste d'objets Fourniture en une liste d'objets FournitureDto
     * @param entities
     * @return
     */
    public List<FournitureDto> mapEntityToDtoList(List<Fourniture> entities){
        if(entities == null){
            return null;
        }
        List<FournitureDto> dtos = new ArrayList<>();
        entities.forEach(element -> dtos.add(mapEntityToDto(element)));
        return dtos;
    }

    /**
     * Mapper (convertit) une liste d'objets FournitureDto en une liste d'objets Fourniture
     * @param dtos
     * @return
     */

    public List<Fourniture> mapDtoToEntityList(List<FournitureDto> dtos){
        if(dtos == null){
            return null;
        }
        List<Fourniture> entities = new ArrayList<>();
        dtos.forEach(element -> entities.add(mapDtoToEntity(element)));
        return entities;
    }

    /**
     * Methode qui prend en entrée un objet FournitureDto et un objet Fourniture et remplit les champs manquants de l'objet FournitureDto avec les données de l'objet Fourniture
     * @param dto
     * @param entity
     * @return
     */
        public FournitureDto populateDto(FournitureDto dto, Fourniture entity){
        if(entity == null){
            return null;
        }
        if(dto.getIdFourniture() == null){
            dto.setIdFourniture(entity.getIdFourniture());
        }
        if(dto.getType() == null){
            dto.setType(entity.getType());
        }
        if(dto.getQuantite() == null){
            dto.setQuantite(entity.getQuantite());
        }
        if(dto.getMagasin() == null){
            dto.setMagasin(MagasinUtils.getInstance()
                    .mapEntityToDto(entity.getMagasin()));
        }
        return dto;
    }
}
