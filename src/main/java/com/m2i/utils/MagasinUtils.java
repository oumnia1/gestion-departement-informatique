package com.m2i.utils;

import com.m2i.dto.FournitureDto;
import com.m2i.dto.MagasinDto;
import com.m2i.entities.Fourniture;
import com.m2i.entities.Magasin;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton en charge des traitements techniques de l'entité et DTO Magasin
 */
public class MagasinUtils {
    private static MagasinUtils instance = null;

    /**
     * Constructeur privé pour empêcher la création d'instances de la classe en dehors de la classe elle-même
     */
    private MagasinUtils() {
    }

    public static MagasinUtils getInstance() {
        if (instance == null)
            instance = new MagasinUtils();

        return instance;
    }

    /**
     * Mapper (convertit) un objet Magasin en MagasinDto
     * @param entity
     * @return
     */
    public MagasinDto mapEntityToDto(Magasin entity){
        if(entity == null){
            return null;
        }
        return MagasinDto.builder()
                .idMagasin(entity.getIdMagasin())
                .nom(entity.getNom())
                //.departement(DepartementUtils.getInstance().mapEntityToDto(entity.getDepartement()))
                .build();
    }

    /**
     * Mapper (convertit) un objet MagasinDto en Magasin
     * @param dto
     * @return
     */
    public Magasin mapDtoToEntity(MagasinDto dto){
        if(dto == null){
            return null;
        }
        return Magasin.builder()
                .idMagasin(dto.getIdMagasin())
                .nom(dto.getNom())
                //.departement(DepartementUtils.getInstance().mapDtoToEntity(dto.getDepartement()))
                .build();
    }

    /**
     * Mapper (convertit) une liste d'objets MagasinDto en une liste d'objets Magasin
     * @param dtos
     * @return
     */

    public List<Magasin> mapDtoToEntityList(List<MagasinDto> dtos){
        if(dtos == null){
            return null;
        }
        List<Magasin> entities = new ArrayList<>();
        dtos.forEach(element -> entities.add(mapDtoToEntity(element)));
        return entities;
    }

    /**
     * Mapper (convertit) une liste d'objets Magasinen une liste d'objets MagasinDto
     * @param entities
     * @return
     */
    public List<MagasinDto> mapEntityToDtoList(List<Magasin> entities){
        if(entities == null){
            return null;
        }
        List<MagasinDto> dtos = new ArrayList<>();
        entities.forEach(element -> dtos.add(mapEntityToDto(element)));
        return dtos;
    }
}
