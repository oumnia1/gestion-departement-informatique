create table role
(
    id_role bigint auto_increment
        primary key,
    nom     varchar(255) null
);

create table utilisateur
(
    id_utilisateur bigint auto_increment
        primary key,
    cin            varchar(255) null,
    email          varchar(255) null,
    fonction       varchar(255) null,
    grade          varchar(255) null,
    nom            varchar(255) null,
    num_bureau     int          null,
    password       varchar(255) null,
    prenom         varchar(255) null,
    tel            varchar(255) null
);

create table departement
(
    id_departement bigint auto_increment
        primary key,
    nom            varchar(255) null,
    adjoint        bigint       null,
    chef           bigint       null,
    technicien     bigint       null,
    constraint FK22iskjt5y9nrkghecqc8q8nft
        foreign key (chef) references utilisateur (id_utilisateur),
    constraint FKd6cq4h5m2lop07c36ja79y1hf
        foreign key (adjoint) references utilisateur (id_utilisateur),
    constraint FKkkr4n1h517idj8y3hok6cmu8y
        foreign key (technicien) references utilisateur (id_utilisateur)
);

create table magasin
(
    id_magasin  bigint auto_increment
        primary key,
    nom         varchar(255) null,
    departement bigint       null,
    constraint FKtnrhxmfkojpbu6v5d2fiyvd27
        foreign key (departement) references departement (id_departement)
);

create table fourniture
(
    id_fourniture bigint auto_increment
        primary key,
    quantite      int          null,
    type          varchar(255) null,
    id_magasin    bigint       null,
    constraint FKlcxqiny114dvek1xdfyhd8nxs
        foreign key (id_magasin) references magasin (id_magasin)
);

create table materiel
(
    id_materiel      bigint auto_increment
        primary key,
    date_acq         date         null,
    date_affectation date         null,
    num_inv          int          null,
    type             varchar(255) null,
    id_magasin       bigint       null,
    affectation      bigint       null,
    constraint FK6kl87xr7mhtpsr6kjcmuaejq9
        foreign key (id_magasin) references magasin (id_magasin),
    constraint FKidqh0j13qsbiayjpf8c2tyri2
        foreign key (affectation) references utilisateur (id_utilisateur)
);

create table t_dep_ens
(
    id_departement bigint not null,
    id_utilisateur bigint not null,
    constraint UK_pldoupxsogbnuhpfvp1wyroq4
        unique (id_utilisateur),
    constraint FK33bdlq9lbs3pfkk75x45rkrjh
        foreign key (id_departement) references departement (id_departement) ON DELETE CASCADE,
    constraint FK9n57aledyuenq8r7jgitem3e0
        foreign key (id_utilisateur) references utilisateur (id_utilisateur) ON DELETE CASCADE
);

create table demande
(
    id_demande      bigint auto_increment
        primary key,
    date_demande    datetime     null,
    date_traitement datetime     null,
    description     varchar(255) null,
    quantite        int          null,
    statut          varchar(255) null,
    type            varchar(255) null,
    approbateur     bigint       null,
    demandeur       bigint       null,
    fourniture      bigint       null,
    materiel        bigint       null,
    constraint FK6wcmw576vide5lhcagss4395s
        foreign key (materiel) references materiel (id_materiel),
    constraint FKaucqgoy8ifd55qo1qk60668gh
        foreign key (approbateur) references utilisateur (id_utilisateur),
    constraint FKberskvdyu48fcqanhpc91igkf
        foreign key (fourniture) references fourniture (id_fourniture),
    constraint FKnw2s5ujaf0ixglh5ktfn20myn
        foreign key (demandeur) references utilisateur (id_utilisateur)
);

create table t_uti_dem
(
    id_utilisateur bigint not null,
    id_demande     bigint not null,
    constraint UK_5iqp6h5tgagry0ho9omi3xxeg
        unique (id_demande),
    constraint FK6uowqh5p2ip435isfcgwiqt89
        foreign key (id_demande) references demande (id_demande)  ON DELETE CASCADE,
    constraint FKqmb10sdc0xvklam5eppdhli1c
        foreign key (id_utilisateur) references utilisateur (id_utilisateur) ON DELETE CASCADE
);

create table t_uti_role
(
    id_utilisateur bigint not null,
    id_role        bigint not null,
    constraint FKfxwwn7r1x73xd93m90ows82mn
        foreign key (id_role) references role (id_role) ON DELETE CASCADE,
    constraint FKk49di6m7nmett8rt7qfq99do7
        foreign key (id_utilisateur) references utilisateur (id_utilisateur) ON DELETE CASCADE
);

