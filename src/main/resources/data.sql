INSERT INTO `gestion-departement-informatique`.departement (id_departement, nom, adjoint, chef, technicien) VALUES (1, 'INFROMATIQUE', null, null, null);

insert into role (id_role, nom) values (1,"ADMINISTRATEUR");
insert into role (id_role, nom)values (2,"UTILISATEUR");

INSERT INTO `gestion-departement-informatique`.utilisateur (id_utilisateur, fonction, cin, grade, nom, num_bureau, prenom, tel, email, password) VALUES (1, 'ENSEIGNANT', 'F234567', 'PES', 'CHOQUET', 27, 'Michael', '09876543','choquet@m2i.com','$2a$10$XWmVIMlTCSkfvgfrtkcDrOG99Svx2y62ypJKiFtFTMAC24ke384n6');
INSERT INTO `gestion-departement-informatique`.utilisateur (id_utilisateur, fonction, cin, grade, nom, num_bureau, prenom, tel, email, password) VALUES (2, 'ENSEIGNANT', 'H321456', 'PH', 'BENOIT', 11, 'Romain', '07823501','benoit@m2i.com','$2a$10$XWmVIMlTCSkfvgfrtkcDrOG99Svx2y62ypJKiFtFTMAC24ke384n6');
INSERT INTO `gestion-departement-informatique`.utilisateur (id_utilisateur, fonction, cin, grade, nom, num_bureau, prenom, tel, email, password) VALUES (3, 'ENSEIGNANT', 'I247831', 'PA', 'FONC', 3, 'Magali', '05112243','fonc@m2i.com','$2a$10$XWmVIMlTCSkfvgfrtkcDrOG99Svx2y62ypJKiFtFTMAC24ke384n6');
INSERT INTO `gestion-departement-informatique`.utilisateur (id_utilisateur, fonction, cin, grade, nom, num_bureau, prenom, tel, email, password) VALUES (4, 'ENSEIGNANT', 'F234789', 'PA', 'CONSTANT', 4, 'Emmanuel', '03178801','constant@m2i.com','$2a$10$XWmVIMlTCSkfvgfrtkcDrOG99Svx2y62ypJKiFtFTMAC24ke384n6');
INSERT INTO `gestion-departement-informatique`.utilisateur (id_utilisateur, fonction, cin, grade, nom, num_bureau, prenom, tel, email, password) VALUES (5, 'ENSEIGNANT', 'D321456', 'PH', 'GLAZER', 10, 'Laurent', '09876543','glazer@m2i.com','$2a$10$XWmVIMlTCSkfvgfrtkcDrOG99Svx2y62ypJKiFtFTMAC24ke384n6');
INSERT INTO `gestion-departement-informatique`.utilisateur (id_utilisateur, fonction, cin, grade, nom, num_bureau, prenom, tel, email, password) VALUES (6, 'ENSEIGNANT', 'A236754', 'PES', 'BOUZGA', 5, 'Oumnia', '043324321','bouzga@m2i.com','$2a$10$XWmVIMlTCSkfvgfrtkcDrOG99Svx2y62ypJKiFtFTMAC24ke384n6');
INSERT INTO `gestion-departement-informatique`.utilisateur (id_utilisateur, fonction, cin, grade, nom, num_bureau, prenom, tel, email, password) VALUES (7, 'TECHNICIEN', 'F357901', 'DEUXIEME', 'SEGRETIN', 21, 'LEO', '015020304','segretin@m2i.com','$2a$10$XWmVIMlTCSkfvgfrtkcDrOG99Svx2y62ypJKiFtFTMAC24ke384n6');

update `gestion-departement-informatique`.departement d set d.adjoint = 1, d.chef = 6, d.technicien = 7 where d.id_departement = 1;

insert into t_uti_role (id_utilisateur, id_role) values (1,2);
insert into t_uti_role (id_utilisateur, id_role) values (2,2);
insert into t_uti_role (id_utilisateur, id_role) values (3,2);
insert into t_uti_role (id_utilisateur, id_role) values (4,2);
insert into t_uti_role (id_utilisateur, id_role) values (5,2);
insert into t_uti_role (id_utilisateur, id_role) values (6,2);
insert into t_uti_role (id_utilisateur, id_role) values (6,1);
insert into t_uti_role (id_utilisateur, id_role) values (2,1);
insert into t_uti_role (id_utilisateur, id_role) values (7,2);

insert into t_dep_ens (id_departement, id_utilisateur) values (1,1);
insert into t_dep_ens (id_departement, id_utilisateur) values (1,2);
insert into t_dep_ens (id_departement, id_utilisateur) values (1,3);
insert into t_dep_ens (id_departement, id_utilisateur) values (1,4);
insert into t_dep_ens (id_departement, id_utilisateur) values (1,5);
insert into t_dep_ens (id_departement, id_utilisateur) values (1,6);

INSERT INTO `gestion-departement-informatique`.magasin (id_magasin, nom, departement) VALUES (1, 'MAGASIN DEP INFO', 1);

INSERT INTO `gestion-departement-informatique`.fourniture (id_fourniture, quantite, type, id_magasin) VALUES (1, 10, 'toner pour imprimante HP 1101', 1);
INSERT INTO `gestion-departement-informatique`.fourniture (id_fourniture, quantite, type, id_magasin) VALUES (2, 100, 'paquet de papier A4', 1);
INSERT INTO `gestion-departement-informatique`.fourniture (id_fourniture, quantite, type, id_magasin) VALUES (3, 37, 'paquet de stylo bleu', 1);
INSERT INTO `gestion-departement-informatique`.fourniture (id_fourniture, quantite, type, id_magasin) VALUES (4, 37, 'paquet de stylo rouge', 1);

INSERT INTO `gestion-departement-informatique`.materiel (id_materiel, date_acq, date_affectation, num_inv, type, affectation, id_magasin) VALUES (1, '2022-09-15', '2022-12-01', 123, 'PC portable', 6, 1);
INSERT INTO `gestion-departement-informatique`.materiel (id_materiel, date_acq, date_affectation, num_inv, type, affectation, id_magasin) VALUES (2, '2022-03-05', '2022-05-01', 1223, 'PC bureau', 5, 1);
INSERT INTO `gestion-departement-informatique`.materiel (id_materiel, date_acq, date_affectation, num_inv, type, affectation, id_magasin) VALUES (3, '2022-03-05', '2022-05-01', 983, 'PC bureau', 4, 1);
INSERT INTO `gestion-departement-informatique`.materiel (id_materiel, date_acq, date_affectation, num_inv, type, affectation, id_magasin) VALUES (4, '2021-03-05', '2022-01-21', 9239, 'disque dur externe', 1, 1);
INSERT INTO `gestion-departement-informatique`.materiel (id_materiel, date_acq, date_affectation, num_inv, type, affectation, id_magasin) VALUES (5, '2021-03-05', '2022-01-21', 825, 'armoire', 3, 1);
INSERT INTO `gestion-departement-informatique`.materiel (id_materiel, date_acq, date_affectation, num_inv, type, affectation, id_magasin) VALUES (6, '2021-03-05', '2022-01-21', 245, 'bureau', 2, 1);
INSERT INTO `gestion-departement-informatique`.materiel (id_materiel, date_acq, date_affectation, num_inv, type, affectation, id_magasin) VALUES (7, '2021-03-05', '2022-01-21', 456745, 'chaise', 2, 1);

insert into demande (id_demande, date_demande, description, statut, type, demandeur, approbateur)
values (1,'2022-09-15','32G de RAM','ENCOURS','MATERIEL',1,null);
insert into demande (id_demande, date_demande, description, statut, type, demandeur, approbateur)
values (2,'2022-09-15','32G de RAM','ENCOURS','MATERIEL',1,null);
insert into demande (id_demande, date_demande, description, statut, type, demandeur, approbateur)
values (3,'2022-09-15','Chaise de bureau','ENCOURS','MATERIEL',1,null);
insert into demande (id_demande, date_demande, description, statut, type, demandeur, approbateur)
values (4,'2022-09-15','Grande bureau','ENCOURS','MATERIEL',1,null);

insert into t_uti_dem (id_utilisateur, id_demande)
values (1,3);
insert into t_uti_dem (id_utilisateur, id_demande)
values (1,4);
insert into t_uti_dem (id_utilisateur, id_demande)
values (2,2);
insert into t_uti_dem (id_utilisateur, id_demande)
values (1,1);
